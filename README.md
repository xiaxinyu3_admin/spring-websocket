# spring-websocket

## 访问方式
Url：http://localhost:9001?sid=123&type=chrome

## session存储模型
- sid 对应多个 session
- 根据sid可以消息进行群发


## 展望
- 如何实现分布式？
- 如何实现数据安全？
- 如何实现鉴权？

